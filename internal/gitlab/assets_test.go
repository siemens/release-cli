package gitlab

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseAssetsNameAndURL(t *testing.T) {
	tests := []struct {
		name           string
		names          []string
		urls           []string
		expectedErrMsg string
	}{
		{
			name:  "empty",
			names: []string{},
			urls:  []string{},
		},
		{
			name:  "one_name_one_url",
			names: []string{"name1"},
			urls:  []string{"url1"},
		},
		{
			name:  "many_names_many_urls",
			names: []string{"name1", "name2", "name3"},
			urls:  []string{"url1", "url2", "url3"},
		},
		{
			name:           "mismatch_name_and_urls_empty_names",
			names:          []string{},
			urls:           []string{"url1"},
			expectedErrMsg: "mismatch length --assets-links-name (0) and --assets-links-url (1) should be equal",
		},
		{
			name:           "mismatch_name_and_urls_empty_urls",
			names:          []string{"name1"},
			urls:           []string{},
			expectedErrMsg: "mismatch length --assets-links-name (1) and --assets-links-url (0) should be equal",
		},
		{
			name:           "mismatch_name_and_urls_many_names",
			names:          []string{"name1", "name2"},
			urls:           []string{"url1"},
			expectedErrMsg: "mismatch length --assets-links-name (2) and --assets-links-url (1) should be equal",
		},
		{
			name:           "mismatch_name_and_urls_many_urls",
			names:          []string{"name1"},
			urls:           []string{"url1", "url2"},
			expectedErrMsg: "mismatch length --assets-links-name (1) and --assets-links-url (2) should be equal",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseAssets(tt.names, tt.urls, nil)
			if tt.expectedErrMsg != "" {
				require.EqualError(t, err, tt.expectedErrMsg)
				return
			}

			require.NotNil(t, got)
			require.Len(t, got.Links, len(tt.names))

			for k, link := range got.Links {
				require.Equal(t, tt.names[k], link.Name)
				require.Equal(t, tt.urls[k], link.URL)
			}
		})
	}
}

func TestParseAssetsLinkJSON(t *testing.T) {
	tests := []struct {
		name           string
		assetsLink     []string
		expectedErrMsg string
	}{
		{
			name:       "empty",
			assetsLink: []string{},
		},
		{
			name:       "one_complete_asset_link",
			assetsLink: []string{`{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy"}`},
		},
		{
			name:       "one_asset_link_some_fields",
			assetsLink: []string{`{"name":"Asset1","url":"https://<domain>/some/location/1"}`},
		},
		{
			name: "many_asset_links",
			assetsLink: []string{`{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}`,
				`{"name":"Asset2","url":"https://<domain>/some/location/2","link_type":"image","filepath":"xzy2"}`,
				`{"name":"Asset3","url":"https://<domain>/some/location/2","link_type":"package","filepath":"xzy3"}`},
		},
		{
			name:           "one_asset_wrong_json",
			assetsLink:     []string{`not_json`},
			expectedErrMsg: fmt.Sprintf("invalid JSON: %q", "not_json"),
		},
		{
			name: "multiple_assets_one_wrong_json",
			assetsLink: []string{`{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy1"}`,
				`not_json`,
				`{"name":"Asset3","url":"https://<domain>/some/location/2","link_type":"package","filepath":"xzy3"}`},
			expectedErrMsg: fmt.Sprintf("invalid JSON: %q", "not_json"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseAssets(nil, nil, tt.assetsLink)
			if tt.expectedErrMsg != "" {
				require.EqualError(t, err, tt.expectedErrMsg)
				return
			}

			require.NoError(t, err)
			if len(tt.assetsLink) == 0 {
				require.Nil(t, got)
				return
			}

			require.NotNil(t, got)
			require.Len(t, got.Links, len(tt.assetsLink))

			for k, link := range got.Links {
				b, err := json.Marshal(link)
				require.NoError(t, err)

				require.JSONEq(t, tt.assetsLink[k], string(b))
			}
		})
	}
}
